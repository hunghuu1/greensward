function ScrollToElement(idclick,idscroll){
    if(typeof idclick === "string" && typeof idscroll ==="string"){
        
        let click = document.querySelector(idclick);
        let scroll = document.querySelector(idscroll);

        if( click && scroll ){
            click.addEventListener("click",function(){
                scroll.scrollIntoView({
                    behavior: "smooth", 
                    block: "start", 
                    inline: "nearest",
                });
            })
        }
    }
}

ScrollToElement("#click","#scroll")


function ClickMenu(idbutton,idmenu){
    if(typeof idbutton === "string" && typeof idmenu ==="string"){
        let button = document.querySelector(idbutton);
        let menu = document.querySelector(idmenu);

        if(button && menu){
            button.addEventListener("click",function(){
                menu.classList.toggle("active")
            })
        }
    }
}

ClickMenu("#button","#menu")